let companys = document.querySelectorAll(".company");
let addBtns = document.querySelectorAll(".add-icon");
let target = document.querySelector("#target");
let count = document.querySelector(".count");
let removeBtns = document.querySelectorAll(".remove-icon");
let companySugg = document.querySelector("#company-sugg");
let companyInfos = document.querySelectorAll(".company-info");
let carets = document.querySelectorAll(".caret");

let accordion = document.querySelectorAll(".my-accordion");

const countryDropdown = document.querySelectorAll(".country-dropdown");
const selectedCountry = document.querySelectorAll(".selected-country");





countryDropdown.forEach((dropdown, i) => {
    dropdown.addEventListener("change", function () {

        const selectedOption = dropdown.options[dropdown.selectedIndex];
        let selectedText = selectedOption.innerHTML;
        let selelectedValue = selectedOption.value;


        let selectContainer = document.createElement("div");
        selectContainer.classList.add("d-flex", "select-country");

        let selectTextDiv = document.createElement("div");
        selectTextDiv.classList.add("col-lg-4");
        let selectTextPara = document.createElement("p");
        selectTextDiv.appendChild(selectTextPara);
        selectTextPara.innerHTML = selectedText;

        let removeIconDiv = document.createElement("div");
        removeIconDiv.classList.add("col-lg-2");
        const removeIcon = document.createElement("i");
        removeIcon.classList.add("fa-solid", "fa-xmark", "remove-country");
        removeIconDiv.appendChild(removeIcon);

        let checkBoxDiv = document.createElement("div");
        checkBoxDiv.classList.add("col-lg-2");
        let checkBox = document.createElement("input");
        checkBox.type = "checkbox";
        checkBoxDiv.appendChild(checkBox);

        let selectValueDiv = document.createElement("div");
        selectValueDiv.classList.add("col-lg-4");
        let selectPara = document.createElement("p");
        selectPara.classList.add("language");
        selectValueDiv.appendChild(selectPara);
        selectPara.innerHTML = selelectedValue;

        selectContainer.appendChild(selectTextDiv);
        selectContainer.appendChild(removeIconDiv);
        selectContainer.appendChild(checkBoxDiv);
        selectContainer.appendChild(selectValueDiv);

        selectedCountry[i].appendChild(selectContainer);


        removeCountryBtns = selectedCountry[i].querySelectorAll(".remove-country");

        removeCountryBtns.forEach((btn, j) => {
            btn.addEventListener("click", () => {
                closeCountry(i, j);

            });
        });




        selectedOption.disabled = true;
        dropdown.selectedIndex = 0;
    });


})


// addbtn accordion 

addBtns.forEach((addBtn, i) => {
    addBtn.addEventListener('click', (e) => {


        if (target.childElementCount > 0) {
            target.lastElementChild.querySelector(".company-info").classList.add("d-none");
            target.lastElementChild.querySelector(".caret").firstElementChild.classList.toggle("rotate-caret")
        }
        target.appendChild(companys[i]);
        companys[i].classList.remove("col-lg-4");
        companys[i].classList.add("col-lg-12");
        addBtn.classList.add("d-none");
        removeBtns[i].classList.remove("d-none");

        companyInfos[i].classList.remove("d-none");
        count.innerHTML = target.childElementCount;
        carets[i].classList.remove("d-none");
        carets[i].firstElementChild.classList.toggle("rotate-caret");



       


    });
});

// remove accordion from select competitor

removeBtns.forEach((removeBtn, i) => {
    removeBtn.addEventListener('click', (e) => {

        companySugg.appendChild(companys[i]);
        companys[i].classList.remove("col-lg-12");
        companys[i].classList.add("col-lg-4");
        removeBtn.classList.add("d-none");
        addBtns[i].classList.remove("d-none");
        companyInfos[i].classList.add("d-none");

    
        count.innerHTML = target.childElementCount;
        carets[i].firstElementChild.classList.remove("rotate-caret");
        carets[i].classList.add("d-none");



        closeAllCountry(i)


    })
})


accordion.forEach((accordion, i) => {

    accordion.addEventListener('click', () => {

        if (target.contains(companyInfos[i])) {
            companyInfos[i].classList.toggle("d-none");
            carets[i].firstElementChild.classList.toggle("rotate-caret");
        }
    })

})




// remove a single country

function closeCountry(i, j) {
    let selectCountryDiv = selectedCountry[i].querySelectorAll(".select-country");
    selectCountryDiv[j].classList.add("d-none");
    //  reanabling   options
    let allOptions = countryDropdown[i].options;

    let selectedLangDiv = selectedCountry[i].querySelectorAll(".language");
    let lang = selectedLangDiv[j].innerHTML;

    for (let i = 1; i < allOptions.length; i++) {

        if (allOptions[i].value === lang) {
            allOptions[i].disabled = false;

        }
    }

}

// remove all country

function closeAllCountry(i) {
    let children = selectedCountry[i].children;

    for (let i = 0; i < children.length; i++) {
        children[i].classList.add("d-none");
    }


    // enabling all options
    let allOptions = countryDropdown[i].options;

    for (let i = 1; i < allOptions.length; i++) {
        allOptions[i].disabled = false;
    }




}


// enabling deleted options

